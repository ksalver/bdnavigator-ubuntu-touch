import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Web 0.2

WebContainerTab {
    title: i18n.tr("Zug pünktlich?")

    startUrl: (mainTabs.selectedTab === this) ? "https://mobile.bahn.de/bin/mobil/trainsearch.exe/dox?webview=&" : ""
}
